Keycloak Role Mapping service
-----------------------------

Keycloak is a very powerful tool for taking various backends and exposing those
as OIDC endpoints. In most circumstances this is fine, but in the situation
where you have multiple backends and want to merge this information together,
keycloak fails to have appropriate in-band tooling.

In particular, when trying to merge two LDAP backends that use differing
versions of RFC2301 you can have trouble.

In order to fix this issue, this service was written to provide a
crontab-compatible service to call the various APIs to gather and merge data.

Be advised - it's been written with three backends, LDAP, keycloak and mysql.
This is specifically because the exposed keycloak API (as of version 20) is
simply too slow to operate, so a more direct query needs to occur.

This might not be forwards-compatible as it uses the raw database structure.


Installation
------------

This repo comes with a setup.py script that creates the specific
dropped-privilege user used to modify the entries in keycloak. This allows
you to not expose more rights than you need to.

Copy config.py.example to config.py to set up your own version for your site.


Operation
---------

The main script map.py is designed to run under a crontab/systemd timer.
Examples are provided.
