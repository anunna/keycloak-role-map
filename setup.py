#!/usr/bin/env python3
import logging
import keycloak_api

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    keycloak_api.setup_rolesync_user()
