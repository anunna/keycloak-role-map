#!/usr/bin/python3
import os
import json
import logging
import ldap3
import config

conn=None
basedn=''
def login():
    '''Do the login stuff. Cache the connection pointer and stash the basedn from the rootDSE.'''
    global conn
    global basedn
    server = ldap3.Server(config.LDAP_HOST,config.LDAP_PORT,use_ssl=config.LDAP_SSL,get_info=ldap3.ALL)
    anon_conn = ldap3.Connection(server,auto_bind=True)
    basedn = server.info.naming_contexts[0]
    conn = ldap3.Connection(server,config.LDAP_BINDDN,config.LDAP_PASSWD,
              client_strategy=ldap3.SYNC,auto_bind=True)

def ldap_search(filter,base=None):
    '''Handle this search nicer than the ldap3 api provides.'''
    if base is None: base = basedn
    logging.debug('ldap search: {} {}'.format(filter,base))
    if conn.search(base, '({})'.format(filter),search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES):
        return conn.entries
    return []

def list_groups():
    '''Get me all valid groups in that group ou.'''
    ret = ldap_search('objectclass=posixGroup',config.LDAP_GROUP_OU)
    logging.debug('ldap list group returned: {}'.format(ret))
    return ret

def list_users():
    '''Get me all valid users in that group ou.'''
    ret = []
    get = ldap_search('objectclass=inetOrgPerson',config.LDAP_USER_OU)
    for u in get:
        if 'uid' in u.entry_attributes:
            ret.append(u['uid'].value)
    logging.debug('ldap list user returned: {}'.format(ret))
    return ret

def get_user_roles():
    '''Make me a dict of shape role:[member] from the groups listed and the members of them.
Eliminate all local ldap users. They have automatic sync.'''
    desired_roles={}
    local_users = list_users()
    for g in list_groups():
        role = g['cn'].values[0]
        if role not in desired_roles: desired_roles[role] = []
        if 'memberUid' in g.entry_attributes:
            for member in g['memberUid']:
                if member not in local_users:
                    desired_roles[role].append(member)
    for role in list(desired_roles.keys())[:]:
        if desired_roles[role] == []: del desired_roles[role]
    return desired_roles
