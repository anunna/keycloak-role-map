#!/usr/bin/python3
import os
import time
import json
import getpass
import logging
from subprocess import Popen, PIPE
import config

def call(args,stdin=None):
    '''Call the keycloak API shell script, but also check if you're logged in.'''
    global expiry
    if time.time() > expiry:
        login()
    return _call(args,stdin)

def _call(args,stdin=None,log=True):
    '''Don't check but execute all subprocess elements. Not to be called outside this file.'''
    if log: logging.info(args)
    else: logging.info('call args hidden for security.')
    args += ['--config',os.path.join(os.path.dirname(os.path.realpath(__file__)),'kcadm.config')]
    shpath=config.KC_PATH+'/bin/kcadm.sh'
    p = Popen([shpath]+args,stdout=PIPE,stderr=PIPE)
    if stdin: o,e = p.communicate(stdin)
    else: o,e = p.communicate()
    logging.debug("STDOUT: {}".format(o))
    logging.debug("STDERR: {}".format(e))
    return o,e

expiry=0
def login(user=None,password=None):
    '''Do the login exchange. Cache the expiry information to login again later.'''
    global expiry
    cmd = ['config','credentials','--server',config.KC_SERVER]
    cmd += ['--realm',config.KC_ADMIN_REALM]
    if not user: user=config.KC_USER
    cmd += ['--user',user]
    logging.info('Logging in to keycloak as {}'.format(user))
    if not password: password = config.KC_PASSWORD
    cmd += ['--password',password]
    _call(cmd,log=False)
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),'kcadm.config')) as f:
        token = json.load(f)
    expiry = token['endpoints'][config.KC_SERVER]['master']['expiresAt']

def setup_rolesync_user():
    '''Designed to be run as master. Uses a different auth to be able to set up the rolesync user properly.'''
    admin_user = input('Admin user for setting up {} :'.format(config.KC_USER))
    admin_passwd = getpass.getpass('Admin password for setting up {} :'.format(config.KC_USER))
    login(admin_user,admin_passwd)
    _call(['create','users','-r',config.KC_ADMIN_REALM,'-s','username={}'.format(config.KC_USER),'-s','enabled=true'])
    _call(['set-password','-r',config.KC_ADMIN_REALM,'--username',config.KC_USER,'-p',config.KC_PASSWORD],log=False)
    o,_ = call(['get','-r',config.KC_ADMIN_REALM,'clients'])
    realms = json.loads(o)
    realm_id = [x['id'] for x in realms if x['clientId'] == '{}-realm'.format(config.KC_REALM)][0]
    _call(['add-roles','-r',config.KC_ADMIN_REALM,'--uusername',config.KC_USER,'--rolename','query-users','--cid',realm_id])
    _call(['add-roles','-r',config.KC_ADMIN_REALM,'--uusername',config.KC_USER,'--rolename','manage-users','--cid',realm_id])
    _call(['add-roles','-r',config.KC_ADMIN_REALM,'--uusername',config.KC_USER,'--rolename','view-identity-providers','--cid',realm_id])
    _call(['add-roles','-r',config.KC_ADMIN_REALM,'--uusername',config.KC_USER,'--rolename','manage-realm','--cid',realm_id])
#Overwrite the login token with the new rolesync user and test it.
    login()

def setup_role_mapping():
    '''Automate the complex task of finding the right box to tick.
This enables keycloak to send realm roles in userinfo and id tokens, allowing clients
to act on this data.'''
    admin_user = input('Admin user for configuring role mapping :')
    admin_passwd = getpass.getpass('Admin password for configuring role mapping :')
    login(admin_user,admin_passwd)
    o,_ = _call(['get','-r',config.KC_REALM,'client-scopes'])
    scopes = json.loads(o)
    scope_id = [x['id'] for x in realms if x['name'] == 'roles'][0]
    o,_ = _call(['get','-r',config.KC_REALM,'client-scopes/{}/protocol-mappers/models'.format(scope_id)])
    mappers = json.loads(o)
    mapper_id = [x['id'] for x in realms if x['name'] == 'realm roles'][0]
    _call(['update','-r',config.KC_REALM,'client-scopes/{}/protocol-mappers/models/{}'.format(scope_id,mapper_id),'-s','config.\"userinfo.token.claim\"=true'])
    _call(['update','-r',config.KC_REALM,'client-scopes/{}/protocol-mappers/models/{}'.format(scope_id,mapper_id),'-s','config.\"id.token.claim\"=true'])
    _call(['update','-r',config.KC_REALM,'client-scopes/{}/protocol-mappers/models/{}'.format(scope_id,mapper_id),'-s','config.\"access.token.claim\"=true'])
    login()

def create_role(name):
    cmd = ['create','-r',config.KC_REALM,'roles','-s','name={}'.format(name)]
    call(cmd)

def delete_role(name):
    cmd = ['delete','-r',config.KC_REALM,'roles/{}'.format(name)]
    call(cmd)

def list_roles():
    cmd = ['get','-r',config.KC_REALM,'roles']
    o,_ = call(cmd)
    try:
        return json.loads(o)
    except:
        return None

def list_users():
    finished=False
    offset=0
    limit=50
    out=[]
    while not finished:
        cmd = ['get','-r',config.KC_REALM,'users','-l',str(limit),'-o',str(offset)]
        o,_ = call(cmd)
        out += json.loads(o)
        offset += limit
        if len(out) == 0: finished = True
    return out

def add_to_role(role,user):
    cmd = ['add-roles','-r',config.KC_REALM,'--uusername',user,'--rolename',role]
    call(cmd)

def remove_from_role(role,user):
    cmd = ['remove-roles','-r',config.KC_REALM,'--uusername',user,'--rolename',role]
    call(cmd)

def list_user_roles(user):
    cmd = ['get-roles','-r',config.KC_REALM,'--uusername',user]
    o,_ = call(cmd)
    try:
        return json.loads(o)
    except:
        return None

def get_user_roles():
    '''Return a dict of shape role:[member]. This isn't actually used because it's so terribly slow.'''
    ignored_roles=['default-roles-{}'.format(config.KC_REALM)]
    role_dict={}
    for user in get_users():
        raw_roles = list_user_roles(user)
        roles = [x['name'] for x in raw_roles['realmMappings'] if x['name'] not in ignored_roles]
        for role in roles:
            if role not in role_dict: role_dict[role]=[]
            role_dict[role].append(user)
    return role_dict
