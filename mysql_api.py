#!/usr/bin/python3
import atexit
import logging
import config
import pymysql

#There's no quick, easy API to get a list of users associated with each role.
#There's only the inverse, and the way to get every user. This is terribly slow.
#So, this code is a workaround. Alas, there's no proper API for this, so it might break.

cur=None
def db_init():
    global cur
    db=pymysql.connect(host=config.MARIADB_HOST,user=config.MARIADB_USER,passwd=config.MARIADB_PASSWD,db=config.MARIADB_DATABASE)
    cur = db.cursor(pymysql.cursors.DictCursor)
    atexit.register(db.close)

def get_user_roles():
    '''Generate a collapsed view of all current users mapped to roles and realms. Only use the ones from the
targetted realm. Return a dict of role:[members]'''
    role_dict={}
    cmd = 'SELECT KEYCLOAK_ROLE.NAME as NAME,USERNAME as USERNAME,REALM.NAME as REALM,COMPONENT.NAME as ORIGIN '
    cmd += 'from USER_ROLE_MAPPING '
    cmd += 'LEFT JOIN KEYCLOAK_ROLE ON USER_ROLE_MAPPING.ROLE_ID=KEYCLOAK_ROLE.ID '
    cmd += 'LEFT JOIN USER_ENTITY ON USER_ENTITY.ID=USER_ROLE_MAPPING.USER_ID '
    cmd += 'LEFT JOIN REALM ON REALM.ID=KEYCLOAK_ROLE.REALM_ID '
    cmd += 'LEFT JOIN COMPONENT ON USER_ENTITY.FEDERATION_LINK=COMPONENT.ID;'
    cur.execute(cmd)
    r = cur.fetchall()
    # Walk through all entries
    for entry in r:
        # Check for correct realm
        if entry['REALM'] == config.KC_REALM:
            # As we want to sync the data from LDAP, we must ignore already present entries from there
            if entry['ORIGIN'] != config.KC_LDAP_NAME:
                # If role isn't present yet, add it to role_dict
                if entry['NAME'] not in role_dict: role_dict[entry['NAME']] = []
                # Add user to role
                role_dict[entry['NAME']].append(entry['USERNAME'])
    return role_dict

