#!/usr/bin/python3
# pylint: disable=missing-docstring
# pylint: disable=consider-using-dict-items
# pylint: disable=logging-format-interpolation
# pylint: disable=consider-using-f-string

import logging
import time
import config
import keycloak_api
import ldap_api
import mysql_api

#Please run this as the associated dropped perm user
def init():
    logging.basicConfig(level=logging.INFO)
    mysql_api.db_init()
    ldap_api.login()

def sync_roles():
    ignored_roles=['default-roles-{}'.format(config.KC_REALM),'uma_protection']
#    current_roles = keycloak_api.get_user_roles()
    current_roles = mysql_api.get_user_roles()
    logging.warning('There are %s mysql roles', len(current_roles))
    desired_roles = ldap_api.get_user_roles()
    logging.warning('There are %s ldap roles', len(desired_roles))
    for k in ignored_roles:
        if k in current_roles:
            del current_roles[k]
        if k in desired_roles:
            del desired_roles[k]
    logging.debug('{} -> {}'.format(current_roles,desired_roles))
    for role in desired_roles:
        if role not in current_roles:
            logging.warning('Creating: {}'.format(role))
            keycloak_api.create_role(role)
            current_roles[role]=[]
        for member in desired_roles[role]:
            if member not in current_roles[role]:
                logging.warning('Adding {} to {}'.format(member,role))
                keycloak_api.add_to_role(role, member)
    for role in current_roles:
        for member in current_roles[role]:
            if role not in desired_roles or member not in desired_roles[role]:
                logging.warning('Removing {} from {}'.format(member,role))
                keycloak_api.remove_from_role(role, member)
        if role not in desired_roles:
            logging.warning('Deleting: {}'.format(role))
            keycloak_api.delete_role(role)

if __name__ == '__main__':
    init()
#    while True:
    sync_roles()
#        time.sleep(config.WAIT_TIME)
